const users = require('./users.json')
const categories = require('./categories.json')
const items = require('./items.json')

module.exports = () => ({
  users: users,
  categories: categories,
  items: items
})
