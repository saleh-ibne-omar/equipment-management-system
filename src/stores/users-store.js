import { defineStore } from 'pinia'
import { api } from 'src/boot/axios'

export const useUsersStore = defineStore('userStore', {
  state: () => {
    return {
      users: [],
      user: {},
      isFetchingUsers: false
    }
  },
  actions: {
    async getAll() {
      this.isFetchingUsers = true
      try {
        const response = await api.get('users')
        const data = await response.data
        this.users = data
        return data
      } catch (ex) {
        return ex
      } finally {
        this.isFetchingUsers = false
      }
    },
    async single(id) {
      this.isFetchingUsers = true
      try {
        const response = await api.get(`users/${id}`)
        const data = await response.data
        this.user = data
        return data
      } catch (ex) {
        return ex
      } finally {
        this.isFetchingUsers = false
      }
    }
  }
})
