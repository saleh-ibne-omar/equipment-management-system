import { defineStore } from 'pinia'
import { api } from 'src/boot/axios'
import { uid } from 'quasar'

export const useCategoriesStore = defineStore('categoriesStore', {
  state: () => {
    return {
      _categories: [],
      isFetchingCategories: false,
      isAdding: false
    }
  },
  actions: {
    async getAll() {
      this.isFetchingCategories = true
      try {
        const response = await api.get('categories')
        const data = await response.data
        this._categories = data
        return data
      } catch (ex) {
        return ex
      } finally {
        this.isFetchingCategories = false
      }
    },
    async add(categoryName) {
      this.isAdding = true
      const categorySlug = categoryName.split(' ').join('-').toLowerCase()
      const category = {
        id: uid(),
        name: categoryName,
        is_deleted: 0,
        slug: categorySlug,
        label: categoryName.toUpperCase()
      }
      try {
        const response = await api.post('categories', category)
        this._categories.push(category)
        return response
      } catch (ex) {
        return ex
      } finally {
        this.isAdding = false
      }
    },
    async remove(id) {
      try {
        const response = await api.delete(`categories/${id}`)
        this._categories = this._categories.filter((category) => category.id != id)
        return response
      } catch (ex) {
        return ex
      }
    },
    async single(id) {
      try {
        const response = await api.get(`categories/${id}`)
        const data = await response.data
        return data
      } catch (ex) {
        return ex
      }
    },
    async update(id, categoryName) {
      const categorySlug = categoryName.split(' ').join('-').toLowerCase()
      try {
        const response = await api.put(`categories/${id}`, {
          name: categoryName,
          slug: categorySlug,
          label: categoryName.toUpperCase()
        })
        // const category = this._categories.find((category) => category.id == id)
        // category.name = categoryName
        // category.slug = categorySlug
        return response
      } catch (ex) {
        return ex
      }
    }
  },
  getters: {
    categories(state) {
      return state._categories
    }
  }
})
