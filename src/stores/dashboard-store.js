import { defineStore } from 'pinia'
import { api } from 'src/boot/axios'

export const useDashboardStore = defineStore('dashboardStore', {
  state: () => {
    return {
      total: {
        users: null,
        categories: null,
        items: null,
        borrows: null
      }
    }
  },
  actions: {
    async getTotalUsers() {
      const response = await api.get('users')
      const data = await response.data
      this.total.users = data.length
    },
    async getTotalCategories() {
      const response = await api.get('categories')
      const data = await response.data
      this.total.categories = data.length
    },
    async getTotalItems() {
      const response = await api.get('items')
      const data = await response.data
      this.total.items = data.length
    },
    async getTotalBorrows() {
      const response = await api.get(`items`)
      const data = await response.data
      this.total.borrows = data.filter((item) => item.user_id != null).length
    },
    getStats() {
      this.getTotalUsers()
      this.getTotalCategories()
      this.getTotalItems()
      this.getTotalBorrows()
    }
  }
})
