import { defineStore } from 'pinia'
import { uid } from 'quasar'
import { api } from 'src/boot/axios'

export const useItemsStore = defineStore('itemsStore', {
  state: () => {
    return {
      _items: [],
      _item: {},
      user_items: [],
      searched_items: [],
      isFetchingItems: false,
      isSubmitting: false
    }
  },
  actions: {
    async getAll(categoryId) {
      this.isFetchingItems = true
      try {
        const response = await api.get(`items?category_id=${categoryId}`)
        const data = response.data
        this._items = data
        return data
      } catch (ex) {
        return ex
      } finally {
        this.isFetchingItems = false
      }
    },
    async remove(id) {
      try {
        const response = await api.delete(`items/${id}`)
        this._items = this._items.filter((item) => item.id != id)
        return response
      } catch (ex) {
        return ex
      }
    },
    async single(id) {
      try {
        const response = await api.get(`items/${id}`)
        const data = response.data
        this._item = data
        this._item.marked_as_discarded = Boolean(Number.parseInt(this._item.marked_as_discarded))
        this._item.marked_as_defective = Boolean(Number.parseInt(this._item.marked_as_defective))
        return _item
      } catch (ex) {
        return ex
      }
    },
    async add(item) {
      this.isSubmitting = true
      const body = { id: uid(), user_id: null, date_borrowed: null, ...item }
      try {
        const response = await api.post('items', body)
        return response
      } catch (ex) {
        return ex
      } finally {
        this.isSubmitting = false
      }
    },
    async update(id, updatedItem) {
      try {
        // const itemsResponse = await api.get('items')
        // const items = itemsResponse.data
        // const item = items.find((item) => item.id == id)
        // updatedItem = { ...item, ...updatedItem }
        const response = await api.patch(`items/${id}`, updatedItem)
        return response
      } catch (ex) {
        return ex
      }
    },
    async byUsers(user_id) {
      this.isFetchingItems = true
      try {
        const response = await api.get(`items?user_id=${user_id}`)
        const data = response.data
        this.user_items = data
        return data
      } catch (ex) {
        return ex
      } finally {
        this.isFetchingItems = false
      }
    },
    async search(q) {
      this.isFetchingItems = true
      try {
        const response = await api.get(`items?code_like=${q}`)
        const data = response.data
        this.searched_items = data.filter((d) => d.user_id == null)
        return data
      } catch (ex) {
        return ex
      } finally {
        this.isFetchingItems = false
      }
    },
    resetSearchItems() {
      this.searched_items = []
    }
  },
  getters: {
    items(state) {
      return state._items
    },
    item(state) {
      return state._item
    }
  }
})
