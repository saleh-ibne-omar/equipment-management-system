const routes = [
  {
    path: '/',
    component: () => import('layouts/CommonLayout.vue'),
    children: [
      { path: '', name: 'dashboard', component: () => import('pages/IndexPage.vue') },
      { path: '/users', name: 'users', component: () => import('pages/UsersPage.vue') },
      {
        path: '/equipments',
        name: 'equipments',
        component: () => import('pages/CategoriesPage.vue')
      }
    ]
  },
  {
    path: '/equipments/:category_id/:category_slug/items',
    component: () => import('layouts/ItemsLayout.vue'),
    children: [
      { path: '', name: 'category_items', component: () => import('pages/ItemsPage.vue') },
      { path: 'add', name: 'add_item', component: () => import('pages/AddItemPage.vue') },
      { path: 'edit/:id', name: 'edit_item', component: () => import('pages/EditItemPage.vue') }
    ]
  },
  {
    path: '/users/:user_id/:user_name/profile',
    component: () => import('layouts/UserProfileLayout.vue'),
    children: [
      { path: '', name: 'user_profile', component: () => import('pages/UserProfilePage.vue') },
      {
        path: 'assign',
        name: 'assign_equipment',
        component: () => import('pages/AssignEquipmentPage.vue')
      }
    ]
  },
  {
    path: '/:catchAll(.*)*',
    component: () => import('pages/ErrorNotFound.vue')
  }
]

export default routes
