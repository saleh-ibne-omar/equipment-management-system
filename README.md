# Quasar Hardware Management (quasar-hardware-management)

Quasar Hardware Management Final Project

Extra: Laravel Back-end of this project [Repository Link]( https://gitlab.com/test1684323/hardware-mys-backend-laravel)

## Install the dependencies
```bash
yarn
# or
npm install
```

### Start the json-server database
```bash
npm run db
```

### Start the app in development mode
```bash
npm run dev
```


### Lint the files
```bash
yarn lint
# or
npm run lint
```


### Format the files
```bash
yarn format
# or
npm run format
```



### Build the app for production
```bash
quasar build
```

### Customize the configuration
See [Configuring quasar.config.js](https://v2.quasar.dev/quasar-cli-vite/quasar-config-js).
